package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/jwt"
	"google.golang.org/api/sheets/v4"

	"strings"
)

type reviewInfo struct {
	Phone             string        `json:"phone"`
	CallRate          float64       `json:"call_rate"`
	CallDuration      string        `json:"call_duration"`
	CallStartDate     string        `json:"call_start_date"`
	CallEndDate       string        `json:"call_end_date"`
	CallOperatorNames []interface{} `json:"call_operator_names"`
	CallId            float64       `json:"call_id"`
}

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	fmt.Println("get client 1")
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	fmt.Println("getTokenFromWeb 1")
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	fmt.Println("token from file 1")
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func writeToSheet(receivedReview *reviewInfo) {

	var privateKeyVar = strings.ReplaceAll(os.Getenv("PRIVATE_KEY"), `\n`, "\n")
	fmt.Println(privateKeyVar)
	var privateKeyIDVar = os.Getenv("PRIVATE_KEY_ID")
	fmt.Println("private key id: " + privateKeyIDVar)

	// Create a JWT configurations object for the Google service account
	conf := &jwt.Config{
		Email:        "reviews@reviews-316819.iam.gserviceaccount.com",
		PrivateKey:   []byte(privateKeyVar),
		PrivateKeyID: privateKeyIDVar,
		TokenURL:     "https://oauth2.googleapis.com/token",
		Scopes: []string{
			"https://www.googleapis.com/auth/spreadsheets",
		},
	}

	client := conf.Client(oauth2.NoContext)

	// Create a service object for Google sheets
	srv, err := sheets.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve Sheets client: %v", err)
	}
	log.Println(receivedReview)

	spreadsheetId := "1W3EsiksCqcVKdtg-Hso7ETchJmlaEFtWYPO-UEckEOg"

	var vr sheets.ValueRange

	writeRange := "A2"

	///this as testStruct

	log.Println(receivedReview.CallOperatorNames)
	var namesForSheet string
	for i, v := range receivedReview.CallOperatorNames {
		log.Println(i)
		namesForSheet = namesForSheet + "\n" + v.(string)
		log.Println(namesForSheet)
	}

	myval := []interface{}{receivedReview.Phone, receivedReview.CallRate, receivedReview.CallDuration, receivedReview.CallStartDate, receivedReview.CallEndDate, namesForSheet, receivedReview.CallId}
	vr.Values = append(vr.Values, myval)
	log.Println(myval)

	_, err = srv.Spreadsheets.Values.Append(spreadsheetId, writeRange, &vr).ValueInputOption("RAW").Do()
	if err != nil {
		log.Fatalf("Unable to retrieve data from sheet. %v", err)
	}
}

/* POST review data */
func postReviewData(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	var receivedReview reviewInfo
	err = json.Unmarshal(body, &receivedReview)
	if err != nil {
		panic(err)
	}
	log.Println(receivedReview)
	log.Println(receivedReview.CallOperatorNames)
	writeToSheet(&receivedReview)
}

/* get port from environment */
func getPort() string {
	var port = os.Getenv("PORT")
	if port == "" {
		port = "8080"
		fmt.Println("INFO: No PORT environment variable detected, defaulting to " + port)
	}
	return ":" + port
}

func main() {
	http.HandleFunc("/", postReviewData)
	err := http.ListenAndServe(getPort(), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}
