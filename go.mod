module gitlab.com/dubovik-a/reviews-to-google-sheet

go 1.13

require (
	golang.org/x/oauth2 v0.0.0-20210615190721-d04028783cf1
	google.golang.org/api v0.48.0
)
